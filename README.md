# IoT-2020-lfg-codebase
---
## Hierarchy of the files
 
Every part of the project should have its own directory under the src/  
src/part1  
scr/part2  

## Architecture
```

```

## Project Parts: ​

```
When we know what needs to be done, let's add project parts and who does what here. 

-- Ready parts:  

-- Known parts:  
Rain detection from the radar data (Vandana)  
ESP8266 NodeMCU MQTT client (Antti)  
MQTT back-end client protocol funtionality (Antti)  


-- Parts that need researched and developers:  
Mapping of coordinates to geotiff pixels  
Physical parts of the rain cover  



```

## Links

#### The Finnish Meteorological Institute

[Accessing data using the Download Service][fmi_access_data]  
[FMI Radar Network][fmi_radar_network]  
[Preliminary info about the radar data][fmi_radar_aws_info]  
[Open data model][fmi_open_data_model]  


[fmi_radar_network]: https://en.ilmatieteenlaitos.fi/fmi-radar-network
[fmi_access_data]: https://en.ilmatieteenlaitos.fi/radar-data-on-aws-s3
[fmi_radar_aws_info]: https://en.ilmatieteenlaitos.fi/radar-data-on-aws-s3
[fmi_open_data_model]: https://en.ilmatieteenlaitos.fi/open-data-manual-data-models

#### NodeMCU

[NodeMCU lua based firmware][nmcu_firm]  
[A beginners guid to ESP8266][nmcu_hardw]  
[NodeMCU pinout][nmcu_pins]  


[nmcu_firm]: https://nodemcu.readthedocs.io/en/release/  
[nmcu_hardw]: https://tttapa.github.io/ESP8266/Chap02%20-%20Hardware.html  
[nmcu_pins]: https://www.make-it.ca/nodemcu-arduino/nodemcu-details-specifications/  


