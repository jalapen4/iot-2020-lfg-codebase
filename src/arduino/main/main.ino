//Libraries
#include <DHT.h>
#include <Stepper.h>
#include <Wire.h>

#define DHTPIN 7     // pin we'll use for the humidity/temp sensor
#define DHTTYPE DHT22   // DHT 22  (AM2302)
DHT dht(DHTPIN, DHTTYPE);

//pins for the stepper
#define STEPPER_PIN_1 9
#define STEPPER_PIN_2 10
#define STEPPER_PIN_3 11
#define STEPPER_PIN_4 12

int step_number = 0; //used to determine stepper phases
float hum;
float temp;
char received;
char current_weather;




//for determining if the cover is open or closed
bool coverState;

bool init_cover(){
  close_cover();
}

void open_cover(){
    int stepCount = 0;
    stepCount = 0;
    coverState = true;
    while (stepCount <= 700){
      OneStep(true);
      delay(2);
      stepCount++;
      }
}

void close_cover(){
    Serial.println("closing cover");
    int stepCount = 0;
    stepCount = 0;
    coverState = false;
    while (stepCount <= 700){
      OneStep(false);
      delay(2);
      stepCount++;
      }
}
//Initializing
void setup() {

  received = '0';
  current_weather = '0';
  coverState = false;
  //i2c bus with address 4
  Wire.begin(4);
  Wire.onRequest(request);
  Wire.onReceive(transmission);
  Serial.begin(9600);
  

  dht.begin();

  pinMode(STEPPER_PIN_1, OUTPUT);
  pinMode(STEPPER_PIN_2, OUTPUT);
  pinMode(STEPPER_PIN_3, OUTPUT);
  pinMode(STEPPER_PIN_4, OUTPUT);
  close_cover();
}



//Turns stepper one step clockwise if dir=true, counter-clockwise if false
void OneStep(bool dir){
    if(dir){
switch(step_number){
  case 0:
  digitalWrite(STEPPER_PIN_1, HIGH);
  digitalWrite(STEPPER_PIN_2, LOW);
  digitalWrite(STEPPER_PIN_3, LOW);
  digitalWrite(STEPPER_PIN_4, LOW);
  break;
  case 1:
  digitalWrite(STEPPER_PIN_1, LOW);
  digitalWrite(STEPPER_PIN_2, HIGH);
  digitalWrite(STEPPER_PIN_3, LOW);
  digitalWrite(STEPPER_PIN_4, LOW);
  break;
  case 2:
  digitalWrite(STEPPER_PIN_1, LOW);
  digitalWrite(STEPPER_PIN_2, LOW);
  digitalWrite(STEPPER_PIN_3, HIGH);
  digitalWrite(STEPPER_PIN_4, LOW);
  break;
  case 3:
  digitalWrite(STEPPER_PIN_1, LOW);
  digitalWrite(STEPPER_PIN_2, LOW);
  digitalWrite(STEPPER_PIN_3, LOW);
  digitalWrite(STEPPER_PIN_4, HIGH);
  break;
} 
  }else{
    switch(step_number){
  case 0:
  digitalWrite(STEPPER_PIN_1, LOW);
  digitalWrite(STEPPER_PIN_2, LOW);
  digitalWrite(STEPPER_PIN_3, LOW);
  digitalWrite(STEPPER_PIN_4, HIGH);
  break;
  case 1:
  digitalWrite(STEPPER_PIN_1, LOW);
  digitalWrite(STEPPER_PIN_2, LOW);
  digitalWrite(STEPPER_PIN_3, HIGH);
  digitalWrite(STEPPER_PIN_4, LOW);
  break;
  case 2:
  digitalWrite(STEPPER_PIN_1, LOW);
  digitalWrite(STEPPER_PIN_2, HIGH);
  digitalWrite(STEPPER_PIN_3, LOW);
  digitalWrite(STEPPER_PIN_4, LOW);
  break;
  case 3:
  digitalWrite(STEPPER_PIN_1, HIGH);
  digitalWrite(STEPPER_PIN_2, LOW);
  digitalWrite(STEPPER_PIN_3, LOW);
  digitalWrite(STEPPER_PIN_4, LOW);
} 
  }
step_number++;
  if(step_number > 3){
    step_number = 0;
  }
}

//Master sends data to slave and this is executed
void transmission(int howMany)
{
  received = Wire.read();
  Serial.println("Received Weather");
  Serial.println(received);
  if (received == '1')
  {
    Serial.println("rain");
  }
  else if (received == '0')
  {
    Serial.println("clear");
  }
}

//When master asks for data, these are provided
void request()
{
  Serial.println("Sending state");
    char humBuff[5];
char tempBuff[10];
char str[20];
    dtostrf(temp,6, 2, tempBuff);
    strcpy(str, tempBuff);
    strcat(str, ",");
    dtostrf(hum, 5, 2, humBuff);
    strcat(str, humBuff);
    strcat(str, ",");
    strcat(str, "0");
  Wire.write(str);
  //Wire.write(coverState);
}

void check_weather()
{
  Serial.println("Checking Weather");
  Serial.println(received);
  Serial.println(current_weather);
  if (received == '1')
  {
    if (current_weather == '0')
    {
      current_weather = '1';
      open_cover();
    }
  }
  else if (received == '0')
  {
    if (current_weather == '1')
    {
      current_weather = '0';
      close_cover();
    }
    else
    {
      if(coverState)
      {
        if(hum<35)
        {
          close_cover();
        }
      }
      else
    {
        if(hum>35)
        {
          open_cover();
        }
    }
      }
    }
  }


//Temperature and humidity are read from the sensor, stored, and printed. 
void loop() {
    temp= dht.readTemperature();
    hum = dht.readHumidity();

    Serial.print("  Temp: ");
    Serial.print(temp);
    Serial.println(" Celsius");
    Serial.print("  Humidity: ");
    Serial.print(hum);

 


 
    //There is a delay before another measurement is performed
    delay(5000);
    check_weather();

    //close_cover();
    //open_cover();
    //Turns the stepper 200 steps to one direction, then to another. Just a placeholder to test movement
    /*
    int stepCount = 0;
    stepCount = 0;
    
    while (stepCount <= 700){
      OneStep(true);
      delay(2);
      stepCount++;
      }
    delay(1000);
    stepCount = 0;  
    while (stepCount <= 700){
      OneStep(false);
      delay(2);
      stepCount++;
    }
   */

}
