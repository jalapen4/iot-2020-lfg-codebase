import asyncio

class RegistrationMixin:
    async def register_device(self, message):
        payload = message["payload"] 
        deviceid = payload["deviceid"]
        if not(deviceid in self._known_devices):
            await self.is_device_known(deviceid)
            await self.subscribe_to_topics(deviceid, "device")
            response_to_device = self.encode_message(message_type="control", message="wait")
            topics = self.device_topics(deviceid)
            await self.publish_to_topic(topics[0][0], response_to_device)
            print("Unknown device", deviceid)
        else:
            if not(self._known_devices[deviceid] == "active"):
                print("Device is known, but not active", deviceid)
            else:
                print("Device is active", deviceid)

    async def is_device_known(self, deviceid):
        request = ("REQ", deviceid, "isknown")
        self.add_device_to_known(deviceid)
        await self.queue_out.put(request)

    async def is_device_known_response_handler(self, message):
        deviceid = message["deviceid"]
        if(deviceid in self._known_devices):
            if(self._known_devices[deviceid] == ("pending" or "inactive")):
                if(message["response"] == "ACK"):
                    self.change_device_status(deviceid, "active")
                    response_to_device = self.encode_message(message_type="control", message="ack")
                    topics = self.device_topics(deviceid)
                    await self.publish_to_topic(topics[0][0], response_to_device)
                elif(message["response"] == "NACK"):
                    self.remove_device_from_known(deviceid)
                    response_to_device = self.encode_message(message_type="control", message="nack")
                    topics = self.device_topics(deviceid)
                    await self.publish_to_topic(topics[0][0], response_to_device)

    def add_device_to_known(self, deviceid, status="pending"):
        self._known_devices[deviceid] = status

    def change_device_status(self, deviceid, new_status):
        print("Device", deviceid, "is now", new_status)
        self._known_devices[deviceid] = new_status

    def remove_device_from_known(self, deviceid):
        print("Device", deviceid, "now removed")
        self._known_devices.pop(deviceid)
