import json

protocol_controller_message_types = {
        "control": "encode_control_message",
        "location": "encode_location_message",
        "weather": "encode_weather_message"
        }
protocol_controller_control_messages = {
        "ack": {"command":"ACK"},
        "nack": {"command":"NACK"},
        "wait": {"command":"WAIT"},
        }
protocol_encoded_message = {
        "deviceid":None,
        "payloadtype":None,
        "payload":None
        }
protocol_encdoded_weather_message_payload = {
        "weather":None
        }
protocol_encdoded_location_message_payload = {
        "command":"LOCATION",
        "location":None
        }

class MessageProtocolMixin:
    def encode_message(self, publisher_id="id0001", message_type=None, message=None):
        assert publisher_id != None, "PROTOCOL encoding: No publisher ID given"
        assert message_type != None, "PROTOCOL encoding: No message type given"
        assert message != None, "PROTOCOL encoding: No message given"
        try:
            method_name = protocol_controller_message_types[message_type] 
        except KeyError as exc:
            raise ValueError('Protocol encoding: Not a valid message type') from exc
        method = getattr(self, method_name)
        encoded_message = method(publisher_id, message)
        return json.dumps(encoded_message)

    def decode_message(self, json_decoded_message=None):
        assert json_decoded_message != None, "PROTOCOL decoding: No message given"
        assert isinstance(json_decoded_message, str), "PROTOCOL decoding: pls give message as str"
        try:
            message=json.loads(json_decoded_message)
        except ValueError as exc:
            raise ValueError("PROTOCOL decoding: input message should be in json") from exc
        return message

    def encode_control_message(self, publisher_id, message):
        encoded_message = protocol_encoded_message
        encoded_message["deviceid"] = self.set_controller_id(publisher_id)
        encoded_message["payloadtype"] = "ctrl"
        try:
            encoded_message["payload"] = protocol_controller_control_messages[message] 
        except KeyError as exc:
            raise ValueError('Protocol encoding: Not a valid control message') from exc
        return encoded_message
        
    def encode_location_message(self, publisher_id, message):
        encoded_message = protocol_encoded_message
        encoded_message["deviceid"] = self.set_controller_id(publisher_id)
        encoded_message["payloadtype"] = "location"
        payload = protocol_encdoded_location_message_payload
        if ("location" in message):
            payload["location"] = message["location"]
            encoded_message["payload"] = payload 
        else:
            raise ValueError("Protocol encoding: Give weather message payload as {\"location\":\"value\"}")
        return encoded_message
        
    def encode_weather_message(self, publisher_id, message):
        encoded_message = protocol_encoded_message
        encoded_message["deviceid"] = self.set_controller_id(publisher_id)
        encoded_message["payloadtype"] = "weather"
        if ("weather" in message):
            encoded_message["payload"] = message 
        else:
            raise ValueError("Protocol encoding: Give weather message payload as {\"weather\":\"value\"}")
        return encoded_message

    def set_controller_id(self, publisher_id):
        return "controller."+publisher_id

