import asyncio
from raincover_message_protocol_mixin import MessageProtocolMixin

class DeviceController(MessageProtocolMixin):
    def __init__(self, mqtt_client, queue_in: asyncio.Queue, interface_queue_out: asyncio.Queue, mqtt_queue_out: asyncio.Queue, controller_id="controller.id0001"):
       
        self.queue_in = queue_in
        self.queue_out = interface_queue_out
        self.mqtt_out = mqtt_queue_out
        self.known_devices = {}
        self.controller_id = controller_id 
        self.client = mqtt_client

    async def init_registration(self):
        while True:
            device_message = await self.queue_in.get()
            payload = device_message["payload"]
            if(payload["deviceid"] in known_devices):
                state = known_devices[device_message["deviceid"]]
                if state == "pending":
                    pass
                elif state == "accepted":
                    pass
            else:
                await self.check_device(device_message["deviceid"])
    
    async def check_device(self, deviceid):
        self.known_devices[deviceid] = "pending"
        request = ("registration", deviceid)
        await self.queue_out.put(request)

    async def complete_registration(self, answer):
        if answer[1] in known_devices:
            await self.serve_request(answer[2])

    async def serve_request(self, message):
        message = self.encode_message(publisher_id = controller_id, message_type="control", message=answer[2])
        await self.mqtt_out.put(message)
        pass

        




            



