import asyncio
from contextlib import AsyncExitStack, asynccontextmanager
from asyncio_mqtt import Client, MqttError

from datetime import datetime

from aio_pika import connect, IncomingMessage

from raincover_message_protocol_mixin import MessageProtocolMixin
from registration_mixin import RegistrationMixin
from controller_topic_mixin import MqttTopicMixin
from weather_mixin import WeatherMixin


class MqttController(MessageProtocolMixin, MqttTopicMixin, RegistrationMixin, WeatherMixin):
    def __init__(self, queue_in: asyncio.Queue, queue_out: asyncio.Queue, hostname, port=1883, password=None, client_id="controller.id0001"):
        self._hostname = hostname
        self._port = port
        self._password = password
        self._client_id = client_id
        
        self._tasks = set()
        self._stack = AsyncExitStack()
        self.queue_in = queue_in
        self.queue_out = queue_out
        
        self._topics = []
        self._topic_filters = []

        self._internal_message_queue = asyncio.Queue()


        self.device_controller_input_queue = asyncio.Queue()
        self.mqtt_out_queue = asyncio.Queue()

        self._known_devices = {}
        self._pending_ext_messages = []
        self._known_locations = []

        self.client = Client(self._hostname, port=self._port, password=self._password, client_id=self._client_id)

    async def publish_to_topic(self, topic, message, retain=False):
        await self.client.publish(topic, message, qos=0, retain=retain)

    async def mqtt_input_message_handler(self, messages):
        async for message in messages:
            message_json=message.payload.decode()
            message = self.decode_message(message_json)
            print("message received: ", message)
            if(message["payloadtype"] == "REQCON"):
                await self.register_device(message)
            elif(message["payloadtype"] == "STATE"):
                await self.queue_out.put(("STATE", message))
                # await self.queue_out.put(('STATE', message['deviceid'], message["payload"]))

    async def external_message_queue_handler(self):
        while True:
            message = await self.queue_in.get()
            if(message["message"] == "RES"):
                 if(message["type"] == "isknown"):
                     await self.is_device_known_response_handler(message)
            elif(message["message"] == "weather"):
                await self.update_weather(message["location"], message["weather"])
            elif(message["message"] == "location"):
                await self.update_location(message["deviceid"], message["location"])
            # if(message[0]=="RES"):
                # if(message[2] == "isknown"):
                    # await self.is_device_known_response_handler(message)
                # elif(message[2] == "location"):
                    # if(message[1] in self._known_devices):
            # elif(message[0] == "weather"):
                # await self.update_weather(message[1], message[2])

    async def start_controller(self):
        async with self._stack:
            self._stack.push_async_callback(self.cancel_tasks, self._tasks)
            self.client = Client(self._hostname, port=self._port, password=self._password, client_id=self._client_id)
            await self._stack.enter_async_context(self.client)
            await self.subscribe_to_topics()
            external_queue_h = asyncio.create_task(self.external_message_queue_handler())
            self._tasks.add(external_queue_h)
            await asyncio.gather(*self._tasks)

    async def cancel_tasks(self, tasks):
        for task in tasks:
            if task.done():
                continue
            task.cancel()
            try:
                await task
            except asyncio.CancelledError:
                pass

async def test_printer(q1: asyncio.Queue, q2: asyncio.Queue) -> None:
    # You can generate a Token from the "Tokens Tab" in the UI

    # write_api = client.write_api(write_options=SYNCHRONOUS)



    while True:
        message = await q1.get()
        if message[0] == "REQ":
            await asyncio.sleep(1)
            await asyncio.sleep(1)
            await q2.put(("RES", message[1], "isknown", "ACK"))
            await asyncio.sleep(2)
            await q2.put(("RES", message[1], "location", "location.finland.oulu"))
            await asyncio.sleep(1)
            await q2.put(("weather", "location.finland.oulu", "RAIN"))
            await asyncio.sleep(2)
            await q2.put(("weather", "location.finland.oulu", "CLEAR"))
        elif message[0]=="STATE":
            print("DEBUG2")
                
            print("DEBUG3")
            # write_api.write(bucket, org, point)
            print("SEND READY")
        await asyncio.sleep(1)



