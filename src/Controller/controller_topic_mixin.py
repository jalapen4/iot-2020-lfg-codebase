import asyncio

rain_cover_topic_types = {
        "registration":"global_registration_topic",
        "device":"set_device_topics",
        "weather":"weather_topics"
        }
class MqttTopicMixin:
    async def subscribe_to_topics(self, topics="broker.global.register", topic_type="registration"):
        if not(topic_type in rain_cover_topic_types):
            raise ValueError("Topic subscribing: incorrect topic_type")
        else:
            method_name = rain_cover_topic_types[topic_type]
            method = getattr(self, method_name)
            await method(topics)

    async def global_registration_topic(self, topics):
        await self.client.subscribe(topics)
        await self.register_filters(topics)

    async def set_device_topics(self, deviceid):
        topics, topic_filters = self.device_topics(deviceid)
        for topic in topics:
            await self.client.subscribe(topic)
        await self.register_filters(topic_filters)

    def device_topics(self, deviceid):
        topics = (
                "device."+deviceid+".ctrl",
                "device."+deviceid+".state"
                )
        topic_filters = (
                "device."+deviceid+".state"
                )
        return topics, topic_filters

    async def weather_topics(self, location):
        await self.client.subscribe(location)

    async def register_filters(self, filters):
        manager = self.client.filtered_messages(filters)
        messages = await self._stack.enter_async_context(manager)
        task = asyncio.create_task(self.mqtt_input_message_handler(messages))
        self._tasks.add(task)

    async def publish_to_topic(self, topic, message, retain=False):
        await self.client.publish(topic, message, qos=0, retain=True)
