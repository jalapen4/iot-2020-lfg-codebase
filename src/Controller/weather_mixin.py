import asyncio

class WeatherMixin:
    async def update_location(self, deviceid, location):
        response_to_device = self.encode_message(message_type="location", message={"location":location})
        topics = self.device_topics(deviceid)
        await self.publish_to_topic(topics[0][0], response_to_device)
        await self.weather_topics(location)

    async def update_weather(self, location, weather):
        weather_message = self.encode_message(message_type="weather", message={"weather":weather})
        await self.publish_to_topic(location, weather_message)
        await asyncio.sleep(4)
        await self.weather_topics(location)
