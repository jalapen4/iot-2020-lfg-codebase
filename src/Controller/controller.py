import asyncio
import json
from asyncio_mqtt import Client, MqttError
from datetime import datetime
from functools import partial
from aio_pika import connect, Message, IncomingMessage
from mqtt_controller import MqttController


class SystemController:
    def __init__(self, q1: asyncio.Queue, q2: asyncio.Queue):
        self.q1 = q1
        self.q2 = q2
        self.test_string = "test string"
        self.connection = None
        self.channel = None
        self.mqtt_output_queue_manager = asyncio.create_task(self.output_queue_manager())
        self.service_channels = asyncio.create_task(self.set_service_channel())

    async def output_queue_manager(self) -> None:
        while True:
            message = await self.q2.get()
            if(message[0] == "REQ"):
                if(message[2] == "isknown"):
                    if(message[1] == 'id0001'):
                        message_dict = {"message": "weather", "lat":"65.059203", "lon":"25.466148"}
                        message_json = json.dumps(message_dict)
                        await self.channel.default_exchange.publish(
                                Message(message_json.encode()),
                                routing_key="weather_request",
                                )
                        await asyncio.sleep(5)
                        await self.q1.put({
                            "message":"RES",
                            "deviceid":"id0001",
                            "type":"isknown",
                            "response":"ACK",
                            }) 
                        await asyncio.sleep(5)
                        await self.q1.put({
                            "message":"location",
                            "deviceid":"id0001",
                            "location":"Linnanmaa",
                            }) 
            elif(message[0] == "STATE"):
                print(message[1])



    async def on_register_message(message: IncomingMessage):
        message_decoded = json.loads(message.body.decode())
        await self.q1.put(message_decoded)

    async def on_weather_message(self, message: IncomingMessage):
        message_decoded = json.loads(message.body.decode())
        await self.q1.put(message_decoded)

    async def set_service_channel(self):
        self.connection = await connect(
            "amqp://guest:guest@localhost/"
        )
        # Creating a channel
        self.channel = await self.connection.channel()
        self.microservice_receive_channel = asyncio.create_task(self.message_receive_channels())

    async def message_receive_channels(self):
        # Declaring queues
        queue_weather_receive = await self.channel.declare_queue("weather_response")
        queue_registration_receive = await self.channel.declare_queue("register_response")

        # Start listening the queues 
        await queue_weather_receive.consume(partial(SystemController.on_weather_message, self), no_ack=True)
        await queue_registration_receive.consume(self.on_register_message, no_ack=True)

async def main():
    
    mqtt_client_id = "controller.id0001"
    mqtt_host = "192.168.1.38"
    # Run the advanced_example indefinitely. Reconnect automatically
    # if the connection is lost.
    reconnect_interval = 3  # [seconds]
    
    queue1 = asyncio.Queue()
    queue2 = asyncio.Queue()
    sysctrl = SystemController(queue1, queue2)
    mqtt_controller_client = MqttController(queue1, queue2, mqtt_host, client_id=mqtt_client_id)
    

    while True:
        try:
            print("connecting")
            await mqtt_controller_client.start_controller()
        except MqttError as error:
            print(f'Error "{error}". Reconnecting in {reconnect_interval} seconds.')
        finally:
            await asyncio.sleep(reconnect_interval)

if __name__ == "__main__":
    asyncio.run(main())

