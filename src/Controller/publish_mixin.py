class publish_mixin:    
    async def publish_to_topic(self, topic, message, retain=False):
        await self.client.publish(topic, message, qos=0, retain=retain)
