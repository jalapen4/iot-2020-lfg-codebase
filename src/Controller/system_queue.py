import asyncio

class ControllerInterfaceQueueMixin:
    async def send_to_interface(self, input_data):
        self.output_queue.put(input_data)
    
    async def read_queue:w(self)
