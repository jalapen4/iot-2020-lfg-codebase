-- register event callbacks for WiFi events
wifi.eventmon.register(wifi.eventmon.STA_CONNECTED, function(station)
        print("\n\tSTATION_CONNECTED")
        print("\tSSID:", station.SSID)
        --gpio.write(config.pin_blue, gpio.LOW)
        device_states.wifi = true
end)

wifi.eventmon.register(wifi.eventmon.STA_GOT_IP, function(station)
        print("\tIP:", station.IP)
        print("\tGATEWAY:", station.gateway)
end)

wifi.eventmon.register(wifi.eventmon.STA_DISCONNECTED, function(station)
        print("\n\tSTATION DISCONNECTED")
        print("\tSSID:", station.SSID)
        print("\treason:", station.reason)
        --gpio.write(config.pin_blue, gpio.HIGH)
        device_states.wifi = false 
end)

--gpio.mode(config.pin_blue, gpio.OUTPUT)

function init_wifi()
    wifi.setmode(wifi.STATION)

    print('\n\nSTATION Mode:',  'mode='..wifi.getmode())

    -- Configure wifi station, auto connection
    station_cfg = {}
    station_cfg.ssid = config.wifi_ssid
    station_cfg.pwd = config.wifi_pass
    station_cfg.save = false
    station_cfg.auto = false
    wifi.sta.config(station_cfg)
    wifi.sta.connect()
end

function test_wifi()
    if wifi.sta.status() == wifi.STA_GOTIP then
        device_states.wifi = true
        return true
    elseif wifi.sta.status() == wifi.STA_CONNECTING then
        return true
    else
        return false
    end
end
