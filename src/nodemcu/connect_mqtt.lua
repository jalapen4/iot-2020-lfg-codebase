-- Function: init MQTT, register callbacks, start connection
-- TODO:
function init_mqtt()
    print("\n\t *** Initialising MQTT client ***")
    mqtt_client = mqtt_init_setup()

    mqtt_client:connect("192.168.1.38", 1883, false, function(client)
        device_states.mqtt = "online"
        g_mqtt_connection_negotiation_state = 1
    end, 
    function(client, reason) 
        device_states.mqtt = "failed"
        print ("\n\t !MQTT: connection could not be established", reason)
    end)
    return mqtt_client
end

function mqtt_init_setup()
    -- keepalive time in seconds
    mqtt_client = mqtt.Client(device_info.Name, 120)
    mqtt_client:lwt("/lwt", "offline", 0, 0)
    mqtt_client:on("connect", function(client) print ("\n\t +MQTT: connected") end)
    mqtt_client:on("connfail", function(client, reason) print ("\n\t !MQTT: connection failed", reason) end)

    mqtt_client:on("offline", function(client)
        device_states.mqtt = "offline"
        print ("\n\t -MQTT: connection offline")
    end)

    mqtt_client:on("message", function(client, topic, data)
        mqtt_incoming_message_handler(topic, data)
        print ("\n\t +MQTT: connection online")
    end)
    return mqtt_client
end
