dofile("config.lua")
dofile("mqtt_message_utils.lua")
dofile("connect_wifi.lua")
dofile("mqtt_communications.lua")
dofile("connect_mqtt.lua")

-- GLOBAL device variables
-- Device states
    device_states = {
        init = false, 
        active = false, 
        mqtt = "offline",
        wifi = false,
    }

    g_mqtt_client = nil
    g_mqtt_connection_negotiation_state = 0
    g_mqtt_broker_register_topic = "broker.global.register"
    g_mqtt_client_ctrl_topic = "device.id0001.ctrl"
    g_mqtt_client_state_topic = "device.id0001.state"

    g_location_topic = nil
    g_temp_state = 0
    g_temp_state_old = 0
    g_hum_state = 0
    g_hum_state_old = 0
    g_cover_state = 0
    g_cover_state_old = 0
    g_weather = false
    g_weather_old = false


    g_i2c_id = 0
    g_sda_pin = 1
    g_scl_pin = 2
    g_addr = 4

function init()

    device_states.init = true

    local init_phase = nil 
    local init_timer = tmr.create()
    local phase_counter = 0

    function setup_wifi()
        if test_wifi() then
            if device_states.wifi then
                init_phase = "mqtt"
                return true
            else
                init_timer:start()
                return false
            end
        else
            init_wifi()
            return false
        end
    end

    function setup_mqtt()
        if test_wifi() then
            g_mqtt_client = init_mqtt()
            init_phase = "finish"
            return true
        else
            return false
        end
    end

    function finish_init()
        print("\n\n *************** FINISHED INIT ********************") 
        init_timer:unregister()
        init_timer = nil
        setup_funtions = nil
        init_phase = nil
        device_states.init =false
        return false
    end

    local setup_functions = {
        wifi = setup_wifi,
        mqtt = setup_mqtt,
        finish = finish_init, 
    }

    init_phase = "wifi"
    local step = setup_functions[init_phase]
    step()

    init_timer:register(7000, tmr.ALARM_SEMI, function()
        step = setup_functions[init_phase]
        while step() do
            step = setup_functions[init_phase]
        end
        if init_timer ~= nil then
            init_timer:start()
        end
    end)
    init_timer:start()
end



function main()

   
    function arduino_weather_update()
        if(g_weather) then
            gpio.write(config.pin_blue, gpio.LOW)
            write_arduino(g_i2c_id, g_addr, '1')
        else
            gpio.write(config.pin_blue, gpio.HIGH)
            write_arduino(g_i2c_id, g_addr, '0')
        end
    end

    function check_state()
        read_arduino(g_i2c_id, g_addr)
        if(g_temp_state ~= g_temp_state_old) then
            print(g_temp_state, g_temp_state_old)
            g_temp_state_old = g_temp_state
            publish_message(g_mqtt_client, "state", "state")
        elseif(g_hum_state ~= g_hum_state_old) then
            print(g_hum_state, g_hum_state_old)
            g_hum_state_old = g_hum_state
            publish_message(g_mqtt_client, "state", "state")
        elseif(g_cover_state ~= g_cover_state_old) then
            print(g_cover_state, g_cover_state_old)
            g_cover_state_old = g_cover_state
            publish_message(g_mqtt_client, "state", "state")
        end
    end

    function check_weather()
        if(g_weather_old ~= g_weather) then
            arduino_weather_update()
            g_weather_old = g_weather
        end
    end

    function tasks()
        if(device_states.active) then
            check_state()
            check_weather()
        else
            if (device_states.mqtt == "online") then
                mqtt_dev_reg(g_mqtt_client)
                return true
            end
        end
    end

    function write_arduino(id, addr, message)
        i2c.start(id)
        i2c.address(id, addr, i2c.TRANSMITTER)
        i2c.write(id, message)
        i2c.stop(id)
    end

    function read_arduino(id, addr)
        i2c.start(id)
        i2c.address(id, addr, i2c.RECEIVER)
        state = i2c.read(id, 14)
        i2c.stop(id)
        g_temp_state = string.sub(state,0,5)
        g_hum_state = string.sub(state,8,11)
        g_cover_state = string.sub(state, 14)
    end

    function init_read_arduino(id,addr)
        print(type(id))
        i2c.start(id)
        i2c.address(id, addr, i2c.RECEIVER)
        state = i2c.read(id, 14)
        i2c.stop(id)
        print(state)
        g_temp_state = string.sub(state,0,5)
        g_hum_state = string.sub(state,8,11)
        g_cover_state = string.sub(state, 14)
        g_temp_state_old = g_temp_state
        g_hum_state_old = g_hum_state
        g_cover_state_old = g_cover_state
    end
    
    if not(i2c.setup(g_i2c_id, g_sda_pin, g_scl_pin, i2c.SLOW)) then
        print("i2c bus init ok")
    end


    init_read_arduino(g_i2c_id, g_addr)

    init()
    gpio.mode(config.pin_blue, gpio.OUTPUT)
    gpio.write(config.pin_blue, gpio.HIGH)

    local main_timer = tmr.create()
    main_timer:register(5000, tmr.ALARM_SEMI,function()
        tasks()
        main_timer:start()
    end)
    main_timer:start()
end

main()
