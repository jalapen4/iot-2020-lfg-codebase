function decode_msg(message)
    if message ~= nil then
        print(message)
        local msg_table = sjson.decode(message)
        return msg_table
    end
end

function publish_message(mqtt_client, message_type, message)
    local message_json = nil
    local message_table = {}
    local retained_message = 0
    local topic = nil

    function to_json()
        message_json = sjson.encode(message_table)
    end

    function construct_state_message()
        message_table.payloadtype = "STATE"
        if message=="waiting" then 
            message_table.payload = "WAITING"
        elseif message=="state" then
            message_table.payload = {
                rainsensor = g_hum_state,
                tempsensor = g_temp_state,
                cover = g_cover_state,
            } 
        end
    end

    function construct_join_message()
        message_table.payloadtype = "REQCON"
        message_table.payload = {deviceid = "id0001",}
    end

    function send_to_topic()
        if mqtt_client:publish(topic, message_json, 0, retained_message) then
            print("\t+pub:", message_type, message)
        else
            print("\n\t!pub:", message_type, message)
        end
    end

    message_table.deviceid = device_info.Name

    if mqtt_client ~= nil then
        if message_type == "state" then
            topic = g_mqtt_client_state_topic
            retained_message = 1
            construct_state_message()
            to_json()
            send_to_topic()
        elseif message_type == "join" then
            topic = g_mqtt_broker_register_topic
            construct_join_message()
            to_json()
            send_to_topic()
        else
            print("\n\t!pub: type missing")
            return false
        end
    end
end

function sub_to_topic(mqtt_client, topics)
    -- remember to use strings
    --
    local mqtt_client_topics = nil

    if(topics == "reg") then 
        mqtt_client_topics = {
            ["device.id0001.ctrl"]=0,
            ["device.id0001.state"]=0,
        }
    elseif(topics == "ctrl_state") then
        mqtt_client_topics = {
            ["broker.global.register"]=0,
            ["broker.global.register1"]=0,
        }
    elseif(topics == "location") then
        mqtt_client_topics = {
            [g_location_topic]=0,
        }
    end

    if not (mqtt_client:subscribe(mqtt_client_topics))  then
        print("\t!sub: Failed to subscibe to ", topics)
        return false
    else
        print("\t+sub: Subscibed to ", topics)
        return true
    end
end

function unsub_from_topic(mqtt_client, topics)
    local mqtt_client_topics = {}

    if(topics == "reg") then 
        mqtt_client_topics = {
            ["broker.global.register"]=0,
            ["broker.global.register1"]=0,
        }
    elseif(topics == "ctrl_state") then
        mqtt_client_topics = {
            ["device.id0001.ctrl"]=0,
            ["device.id0001.state"]=0,
        }
    elseif(topics == "location") then
        mqtt_client_topics = {[g_location_topic] = 0} 
    end

    if not (mqtt_client:unsubscribe(mqtt_client_topics))  then
        print("\t!unsub: Failed to unsubscibe from ", topics)
        return false
    else
        print("\t-unsub: Unsubscibed from ", topics)
        return true
    end
end
