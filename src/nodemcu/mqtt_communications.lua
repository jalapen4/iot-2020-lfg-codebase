--- Function: subscribe to the global registration channel and publish a request for registration
--      input: MQTT client ojbect
--      ouput: None
-- TODO: 
function mqtt_dev_reg(mqtt_client)
    sub_to_topic(mqtt_client, "reg")
    publish_message(mqtt_client, "join", "join")
    unsub_from_topic(mqtt_client, "reg")
    sub_to_topic(mqtt_client, "ctrl_state")
    device_states.mqtt = "negot"
end

function mqtt_incoming_message_handler(topic, data)

    local message = nil

    function data_accept()
        if(data ~= nil) then
            message = decode_msg(data)
            print(message.payloadtype)
            if (message.deviceid == "controller.id0001") then
                if(message.payload ~= nil) then 
                    if(message.payloadtype == "ctrl" or message.payloadtype == "location" or message.payloadtype == "weather") then
                        return true
                    end
                end
            end
        end
        return false
    end

    function topic_accept()
        if not (topic=="broker.global.register" or topic=="device.id0001.state") then
            return true
        else
            return false
        end
    end
    
    function select_handler()
        if message.payloadtype == "ctrl" then
            mqtt_control_handler(g_mqtt_client, topic, message.payload)
        elseif message.payloadtype == "location" then
            mqtt_location_handler(g_mqtt_client, topic, message.payload)
        elseif message.payloadtype == "weather" then
            mqtt_weather_handler(g_mqtt_client, topic, message.payload)
        end
    end

    if topic_accept() then
        if data_accept() then
            select_handler()
        end
    end

end

function mqtt_weather_handler(mqtt_client, topic, data)
    if data.weather == "RAIN" then
        print("rain!!!!!!!!!!!")
        g_weather = true
    else
        g_weather = false
    end
end

function mqtt_control_handler(mqtt_client, topic, data)
    function action()
        print(data.command)
        if (data.command == "WAIT") then
            if (g_mqtt_connection_negotiation_state == 1) then
                publish_message(mqtt_client, "state", "waiting")
                g_mqtt_connection_negotiation_state = 2
            end
        elseif(data.command == "ACK") then
            if (g_mqtt_connection_negotiation_state == 1 or g_mqtt_connection_negotiation_state == 2) then
                publish_message(mqtt_client, "state", "state")
                g_mqtt_connection_negotiation_state = 3
                device_states.active = true
            end
        elseif(data.command == "NACK") then
            g_mqtt_connection_negotiation_state = 4
        elseif(data.command == "CLOSE") then
            if (g_mqtt_connection_negotiation_state == 3) then
                print("Closing")
            end
        elseif(data.command == "OPEN") then
            if (g_mqtt_connection_negotiation_state == 3) then
                print("Opening")
            end
        elseif(data.command == "SENDSTATE") then
            publish_message(mqtt_client, "state", "state")
        end
    end

    action()

end

function mqtt_location_handler(mqtt_client, topic, data)
    function set_location()
        if (g_mqtt_connection_negotiation_state == 3) then
            if g_location_topic == nil then
                g_location_topic = data.location
                sub_to_topic(mqtt_client, "location")
                print("\n\t Subscribed to ", g_location_topic)
            else
                unsub_from_topic(mqtt_client, "location") 
                if not tmr.create():alarm(3000, tmr.ALARM_SINGE, function()
                    g_location_topic = data.location
                    sub_to_topic(mqtt_client, "location")
                end)
                then
                    print("\t LOCATION UPDATE FAILED")
                end
            end
        end
    end
    set_location()
end

function mqtt_connection_termination_handler(mqtt_client, topic, data)
end

