import rasterio
from rasterio.plot import show
from matplotlib import pyplot

with rasterio.open('show-4.tif') as src:
    pyplot.imshow(src.read(1), cmap='terrain' )
    # Linnanmaa 2590, 3335
    pyplot.annotate('OULU', xy=(2590, 3335), xycoords='data',
            xytext=(0.54, 0.54), textcoords='figure fraction',
            arrowprops=dict(arrowstyle="->"))
    pyplot.show()

