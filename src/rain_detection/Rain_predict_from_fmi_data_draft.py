#!/usr/bin/env python
# coding: utf-8

# In[1]:
import threading
import asyncio
from aio_pika import connect, Message, IncomingMessage
from functools import partial
import json

import requests
import shutil

import rasterio
from datetime import datetime, timedelta
import pytz

from pyproj import Proj, Transformer
from geopy.geocoders import Nominatim


# In[2]:
class RainPredictor:

    def __init__(self, update_interval):
        self._update_interval = update_interval
        self._known_locations = {}
        self._known_location_suburbans = {}
        self._geolocator = Nominatim(user_agent = "geoLocator")
        self._transformer = Transformer.from_crs("epsg:4326", "epsg:3067")
        self.image_index =0
    
    def run(self):
        self.rain_detection()
        threading.Timer(self._update_interval, self.run).start()
        

    def add_location(self, lat, lon):
        suburb=self.suburban_location(lat, lon)
        pixel_x, pixel_y = self.location_transformation(lat, lon)
        self.update_dict_pixel_coordinates(pixel_x, pixel_y)
        self.update_suburb(pixel_x, pixel_y, suburb)
        self.rain_detection()
        return (self._known_locations[(pixel_x, pixel_y)], self._known_location_suburbans[(pixel_x, pixel_y)])

    def suburban_location(self, x, y):
        # location = self._geolocator.reverse((x,y))
        # address = location.raw["address"]["suburb"]
        address = "Linnanmaa"
        return address

    #function to convert real coordinates to pixel
    def location_transformation(self, lat, lon):
        x,y = self._transformer.transform(lat, lon)
        return x, y

    def update_suburb(self, x_lat, y_lon, suburb):
        key=(x_lat,y_lon)
        if not (key in self._known_location_suburbans):
            self._known_location_suburbans[key] = suburb 
    # In[3]:
    def update_dict_pixel_coordinates(self, x_lat, y_lon):
        key=(x_lat,y_lon)
        if not (key in self._known_locations):
            self._known_locations[key] = False 
    #rain detection method

    #it takes the dictinary which has key-value pair of pixels coordinates and status of rain(as boolean)
    #predicts rain and if there is rain sets true for that particular pixel and returns updated dictionary

    def get_latest_radar_geotiff(self):
        endtime = datetime.now(pytz.timezone('Europe/Helsinki')) - timedelta(hours=2,minutes=30)
        endtime=endtime.strftime('%Y-%m-%d %H:%M:%S')
        
        #round the current datetime by 5 mins
        roundTo=300
        dt=datetime.strptime(endtime, '%Y-%m-%d %H:%M:%S')
        seconds = (dt.replace(tzinfo=None) - dt.min).seconds
        rounding = (seconds+roundTo/2) // roundTo * roundTo
        end= dt + timedelta(0,rounding-seconds,-dt.microsecond)
        time_it=end
        

        filename_start='FIN-DBZ-3067-250M/'
        filename_end='_FIN-DBZ-3067-250M.tif'

        key = '{year}/{month}/{day}/{prefix}{stamp}{suffix}'.format(year=time_it.strftime('%Y'),
                                                                    month=time_it.strftime('%m'),
                                                                    day=time_it.strftime('%d'),
                                                                    stamp=time_it.strftime('%Y%m%d%H%M'),
                                                                    prefix=filename_start,
                                                                    suffix=filename_end)
        
        url = 'http://s3-eu-west-1.amazonaws.com/fmi-opendata-radar-geotiff/'+key

        response = requests.get(url, stream=True)
        with open('img.tif', 'wb') as out_file:
            shutil.copyfileobj(response.raw, out_file)
        del response

    def rain_detection(self):    
        self.get_latest_radar_geotiff()
        
        old_values = self._known_locations
        old_suburb = self._known_location_suburbans
        
        timestep=300
        for i in self._known_locations:
            x_pix=i[0]
            y_pix=i[1]

            images = ('show-0.tif','show-1.tif','show-2.tif','show-3.tif','show-4.tif','show-5.tif')
            print("Image file: ", images[self.image_index])
            with rasterio.open(images[self.image_index]) as src:
                for val in src.sample([(x_pix, y_pix)]):
                    # From https://en.ilmatieteenlaitos.fi/open-data-manual-radar-data
                    prec_sum= val[0]*.01*timestep/3600
                    # print(prec_sum)
                    if prec_sum>0:
                        self._known_locations[i]=True
                        #change rain_flag_i to true
                    else:
                        self._known_locations[i]=False
            self.image_index = self.image_index + 1
            if(self.image_index == 6):
                    self.image_index = 0

        # print(self._known_locations)

        


# In[6]:

# async def on_weather_request(rp, message: IncomingMessage):
    # print(message.body)
# async def on_test(message: IncomingMessage):
    # print(message.body)

# async def request_manager(rain_predictor, channel):
    # queue_weather_receive = await channel.declare_queue("hello")
    # # await queue_weather_receive.consume(partial(on_weather_request, rain_predictor), no_ack=True)
    # await queue_weather_receive.consume(on_test, no_ack=True)
    # print("hello")

async def send_weather(rp):
    while True:
        print("sending weathern in 10s")
        await asyncio.sleep(10)
        for key in rp._known_locations:
            location = rp._known_location_suburbans[key]
            weather = rp._known_locations[key] 
            await weather_response(location, weather)

async def weather_response(location, weather):

    print("Weather: ", weather)
    print("Location: ", location)
    if(weather):
        weather = "RAIN"
    else:
        weather = "CLEAR"
    message_dict = {"message": "weather", "location":location, "weather":weather}
    message_json = json.dumps(message_dict)
    connection = await connect(
            "amqp://guest:guest@localhost/", loop=loop
            )
    channel = await connection.channel()
    await channel.default_exchange.publish(
            Message(message_json.encode()),
            routing_key="weather_response"
            )
    await connection.close()

async def on_request(rp, message: IncomingMessage):
    message_decoded = json.loads(message.body.decode())
    weather, location = rp.add_location(float(message_decoded['lat']), float(message_decoded['lon']))
    await weather_response(location, weather)

async def main(loop):

    #Currently, for testing 
    connection = await connect(
            "amqp://guest:guest@localhost/", loop=loop
            )
    channel = await connection.channel()
  
    update_interval = 15
    rp = RainPredictor(update_interval)
    
    queue = await channel.declare_queue("weather_request")
    await queue.consume(partial(on_request, rp), no_ack=True)
    send_manager = asyncio.create_task(send_weather(rp))

    #Rovaniemi 66.5039  25.7294
    #oulu 65.0121  25.4651
    #oulu, linnanmaa 65.059203, 25.466148
    #turku 60.4518  22.2666

    print("Running")
    rp.run()

if __name__=="__main__":
    loop = asyncio.get_event_loop()
    loop.create_task(main(loop))
    loop.run_forever()
